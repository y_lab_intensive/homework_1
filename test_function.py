from Project_1 import *

# ---------------
#     Test_task_1
# ---------------
print("----Test_task_1----")
print("1. domain_name(\"http://www.zombie-bites.com\") == \"zombie-bites\"")
print(domain_name("http://www.zombie-bites.com") == "zombie-bites")
print("2. domain_name(\"http://google.com\") == \"google\"")
print(domain_name("http://google.com") == "google")
print("3. domain_name(\"http://google.co.jp\") == \"google\"")
print(domain_name("http://google.co.jp") == "google")
print("4. domain_name(\"www.xakep.ru\") == \"xakep\"")
print(domain_name("www.xakep.ru") == "xakep")
print("5. domain_name(\"https://youtube.com\") == \"youtube\"")
print(domain_name("https://youtube.com") == "youtube", '\n')

# ---------------
#     Test_task_2
# ---------------
print("----Test_task_2----")